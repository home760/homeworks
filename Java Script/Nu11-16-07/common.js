// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем – повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою – інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно – іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються – вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.


form = document.querySelector('.password-form')
form.addEventListener('click', event => {
    
    let inputId = event.target.getAttribute('data-info')
    if (inputId) {
        event.target.classList.toggle('fa-eye-slash')       
        let input = form.querySelector(inputId) 
    
      if (event.target.classList.contains('fa-eye-slash')) {
            input.type = 'text'
        } else if (!event.target.classList.contains('fa-eye-slash')) { 
                input.type = 'password' 
}    
    }
   
    if (event.target.tagName === 'BUTTON') {
    let firstInput = form.querySelector('#first-eye')
    let secondInput = form.querySelector('#second-eye')
    let error = form.querySelector('.error')
         if (firstInput.value !== secondInput.value || firstInput.value === '' || secondInput.value === '')
         {error.style.opacity = 1}
         else if (firstInput.value === secondInput.value) {
             error.style.opacity = 0
             alert('You are welcome')
        }
    }
})






// ЗАКОМЕНТОВАНІ ЩЕ ДВА ВАРІАНТИ ВИКОНАННЯ ДОМАШКИ






// let form = document.querySelector('.password-form')
// form.addEventListener('click', (event) => {
//     let eyeId = event.target.dataset.info
//     let input = form.querySelector(eyeId)
//     let i = form.getElementsByTagName('i')
//     for (let item of i) {
//         if (item.classList.contains(eyeId)) {
//             item.classList.toggle('fa-eye-slash')
//             if (item.classList.contains('fa-eye-slash')) {
//         input.type = 'text'
//         } else {input.type = 'password' }
//         }
//     }
//     let firstPassword = form.querySelector('#first-eye')
//     let secondPassword = form.querySelector('#second-eye')
//     let error = form.querySelector('.error')
//     if (event.target.tagName === 'BUTTON') {
//          if (firstPassword.value !== secondPassword.value || firstPassword.value === '' || secondPassword.value === '')
//          { error.style.opacity = 1 }
//          else if (firstPassword.value === secondPassword.value) {
//              console.log(error.style.opacity = 0)
//              alert('You are welcome')
//         }
//     }
// })




// let eyeFirst = document.querySelector('.icon-password-first');
// let inputFirst = document.querySelector('.input_first');
// eyeFirst.addEventListener('click', (event) => {
//     eyeFirst.classList.toggle('fa-eye-slash')
//     if (eyeFirst.classList.contains('fa-eye-slash')) {
//         inputFirst.type = 'text' 
//     } else {inputFirst.type = 'password' }
// })

// let eyeSecond = document.querySelector('.icon-password-second');
// let inputSecond = document.querySelector('.input_second');
// eyeSecond.addEventListener('click', (event) => {
//     eyeSecond.classList.toggle('fa-eye-slash')
//         if (eyeSecond.classList.contains('fa-eye-slash')) {
//             inputSecond.type = 'text' 
//         } else {inputSecond.type = 'password' }
// })

// let buttonConfirm = document.querySelector('.button_confirm');
// buttonConfirm.addEventListener('click', () => {
//     if (inputFirst.value === inputSecond.value) {
//         alert('You are welcome')
//     } else {alert('Потрібно ввести однакові значення')}
// })