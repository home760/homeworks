// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.



function tabs() {
    let btn = document.querySelectorAll('.tabs-title ')
    btn.forEach(function (item) {
        let activeBtn = item;
         

        activeBtn.addEventListener('click', () => {
            btn.forEach((item) => item.classList.remove('active'))
            activeBtn.classList.add('active')


 let contentId = activeBtn.getAttribute('data-btn')
                        
            let content = document.querySelector(contentId)
            let allConent = document.querySelectorAll('.text-content')
            allConent.forEach((item) => item.classList.remove('active'))

            content.classList.add('active')
                        
        }) 
        
    })
}
tabs() 