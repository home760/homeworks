// При завантаженні сторінки показати користувачеві поле вводу (input) з написом Price. Це поле буде служити для введення числових значень
// Поведінка поля має бути наступною:
// При фокусі на полі вводу – має з’явитися рамка зеленого кольору. У разі втрати фокусу вона пропадає.
// Коли прибрано фокус з поля – його значення зчитується, над полем створюється span, у якому має бути текст: Поточна ціна: ${значення з поля вводу}. Поруч із ним має бути кнопка з хрестиком (X). Значення всередині поля вводу забарвлюється у зелений колір.
// При натисканні на Х – span з текстом та кнопка X повинні бути видалені. Значення, введене у поле вводу, обнулюється.
// Якщо користувач ввів число менше 0 – при втраті фокусу підсвічувати поле введення червоною рамкою, під полем виводити фразу – Please enter correct price. span з некорректним значенням при цьому не створюється.
// У папці img лежать приклади реалізації поля вводу та “span”, які можна брати як приклад

  let wrapper = document.querySelector('.list_cancell_wrapper')

  let error = document.querySelector('.error')
  let input = document.querySelector('#input')


input.addEventListener('focusin', event => {
  error.style.opacity = '0'
  input.style.cssText = `
  border: none;
  outline: none;
  border: 2px solid green;
  box-sizing: border-box;
`
})

input.addEventListener('focusout', event => {
  input.style.border = '1px solid black'



  if (input.value === '') {
    input.style.border = '2px solid red'
    error.style.opacity = '1'
    error.style.color = 'red'

 
  } else if (isNaN(input.value) ||input.value <= 0) {
    input.style.border = '2px solid red'
    error.style.opacity = '1'
    error.style.color = 'red'
  } else {
      let cancel = document.createElement('div')
     let cell = document.createElement('div')   
    cell.innerText = `Поточна ціна: ${input.value}`
    cell.style.cssText = `
    padding: 5px 20px;
    border: 1px solid grey;
    border-radius: 15px;
    width: 200px;
    display: grid;
     grid-template-rows: 1fr;
     grid-template-columns: 6fr 1fr;
    
    `
  wrapper.append(cell)
input.value = ''
  cancel.classList.add('cancel')
  cancel.innerText = 'x'
  cancel.style.cssText = `
    padding-left: 3px
    font-weight: 600;
    cursor: pointer`
 
  cancel.addEventListener('click', event => {
    if (event.target === cancel)
      console.log(event.target.classList = 'cancel')
event.target.parentElement.remove()}) 
  
 cell.append(cancel)

    
    
  }

 
  





})