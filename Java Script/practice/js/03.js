// Kids drink toddy.
// Teens drink coke.
// Young adults drink beer.
// Adults drink whisky.
// Make a function that receive age, and return what they drink.

// Rules:

// Children under 14 old.
// Teens under 18 old.
// Young under 21 old.
// Adults have 21 or more.

let age = prompt("How old are you?")
function drinkChoice(age) {
    if (age < 14) {
        alert('Kids drink toddy.')
    } else if (age >= 14 && age < 18) {
        alert('Teens drink coke.')
    } else if (age >= 18 && age < 21) {
        alert('Young adults drink beer.')
    } else if (age >= 21) {
        alert('Adults drink whisky.')
    }
}
console.log(drinkChoice(age))