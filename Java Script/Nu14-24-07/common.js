

let darkMode = document.querySelector('.dark_mode')
if (localStorage.getItem('theme') === 'dark') {
    document.body.classList.add('dark')
}



darkMode.addEventListener('click', () => {
    document.body.classList.toggle('dark')
if (document.body.classList.contains('dark')) {
   localStorage.setItem('theme', 'dark') 
} else if (!document.body.classList.contains('dark')) {
    localStorage.clear()
}     
})


