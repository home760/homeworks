// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.


// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent – DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
// Приклади масивів, які можна виводити на екран:

// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.

// Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву: ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]; Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
// Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.



let arrList = ["hello", "world", ["1", "2", "3", "sea", "user", 23], "Kiev", "Kharkiv", "Odessa", "Lviv"];

function createList(arrList, parent = document.body) {
    const ul = document.createElement('ul')
    ul.style.marginTop = '100px'

    for (let element of arrList) {
        if (!Array.isArray(element)) {
            let liFromArray = document.createElement('li')
            liFromArray.innerText = element   
        ul.append(liFromArray)
    }
  
        let secondUl = document.createElement('ul')
        
    if (Array.isArray(element)) {
        element = element.map(item => { 
            let liSecond = document.createElement('li')   
            liSecond.innerText = item
            secondUl.append(liSecond)
        })
        ul.append(secondUl)
        }
    }
    parent.append(ul)
   
     
    
 setTimeout(() => ul.remove(), 4000);    
    let sec = 3;

setInterval(tick,1000)
    function tick() {
        if (sec > 0) {
            
            let time = document.createElement('p')
            time.innerText = sec--
          ul.before(time)
          time.replaceWith(time.innerText)
         
      }
        
     
}

  
}
createList(arrList)



