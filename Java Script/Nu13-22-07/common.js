// У папці banners лежить HTML код та папка з картинками.
// При запуску програми на екрані має відображатись перша картинка.
// Через 3 секунди замість неї має бути показано друга картинка.
// Ще через 3 секунди – третя.
// Ще через 3 секунди – четверта.
// Після того, як будуть показані всі картинки – цей цикл має розпочатися наново.
// Після запуску програми десь на екрані має з’явитись кнопка з написом Припинити.
// Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
// Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.


let img = document.querySelectorAll('.image-to-show');
let start = document.querySelector('.start');
let pause = document.querySelector('.pause');
let finish = document.querySelector('.finish');

let timer
let index = 0
let arr = Array.from(img)
console.log(arr)

function time() {
    start.setAttribute('disabled', '') 
    timer = setInterval(() => {
    
arr.forEach((element) => element.classList.remove('active'))
    index++
    if (index >= arr.length) {
         index = 0
    }
    let item = arr[index]
    item.classList.add('active')
    
}, 3000)   

    pause.addEventListener("click", () => {
    start.removeAttribute('disabled')     
    clearInterval(timer);
    
    
})  
    finish.addEventListener('click', () => {
    start.removeAttribute('disabled') 
     clearInterval(timer);
    arr.forEach((element) => element.classList.remove('active'))
    arr[0].classList.add('active')
    
    })   
    
    
   
}
time() 


start.addEventListener('click', time)
