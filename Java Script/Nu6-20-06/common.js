// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з’єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об’єкта.





function createNewUser() {

      const newUser = {
            name: "",
            lastName: '',
            birthday: null,
            getAge() {
                  let age = Math.floor((new Date() - this.birthday)/(365*24*60*60*1000));
                  return age;
            },
            getLogin() {
                  let login = (this.name.charAt(0) + this.lastName).toLowerCase();
                  return login;
            },      
            getPassword() {
                  let password = this.name.slice(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
                  return password;
            },

      }

      const name = prompt('What is your name?')
      if (!isNaN(name) || name === null || name === undefined) {
            alert('Error')
      }  newUser['name'] = name; 
      const lastName = prompt('What is your surname?')
      if (!isNaN(lastName) || lastName === null || lastName === undefined) {
            alert('Error')
      } newUser['lastName'] = lastName; 

      let birth = prompt('Your date of birth?');
      birth = birth.split('.');
      newUser.birthday = new Date(birth[2], birth[1] - 1, birth[0])



return newUser

}


const newUser = createNewUser()
console.log(newUser)
console.log(newUser.getLogin())
console.log(newUser.getAge())
console.log(newUser.getPassword())

     