// У файлі index.html лежить розмітка для кнопок.
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш – та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір – вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.


  
let button = document.getElementsByClassName('btn')
window.addEventListener('keyup', (event) => {

    for (let item of button) {
        let command
        if (item.innerText === 'Enter') {
            command = item.innerText
        } else { command = 'Key' + item.innerText }
        if (event.code == command) {
            item.classList.add('active')
        } else {
            item.classList.remove('active')
        }
        
    }
})
