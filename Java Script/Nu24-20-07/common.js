// Намалювати на екрані поле 8*8 (можна використовувати таблицю чи набір блоків).
// Згенерувати на полі випадковим чином 10 мін. Користувач не бачить, де вони знаходяться.
// Клік лівою кнопкою по осередку поля “відкриває” її вміст користувачеві.
// Якщо в цій клітинці знаходиться міна, гравець програв. У такому разі показати всі інші міни на полі. Інші дії стають недоступними, можна лише розпочати нову гру.
// Якщо міни немає, показати цифру – скільки мін знаходиться поруч із цїєю клітинкою.
// Якщо клітинка порожня (поряд з нею немає жодної міни) – необхідно відкрити всі сусідні клітинки з цифрами.
// Клік правої кнопки миші встановлює або знімає із “закритої” клітинки прапорець міни.
// Після першого ходу над полем має з’являтися кнопка “Почати гру заново”, яка обнулюватиме попередній результат проходження та заново ініціалізуватиме поле.
// Над полем має показуватись кількість розставлених прапорців, та загальна кількість мін (наприклад 7 / 10).



let wrapper = document.createElement('div');
wrapper.style.cssText = `
width: 800px;
height: 800px;
border: 1px solid black;
display: flex;
flex-flow: row wrap;
box-sizing: content-box;
`
let wrapperSmall = document.createElement('div');
wrapperSmall.style.cssText = `
width: 200px;
height: 200px;
border: 1px solid black;
display: flex;
flex-flow: row wrap;
box-sizing: content-box;
`




document.body.append(wrapper);
document.body.append(wrapperSmall);

for (let i = 0; i < 256; i++){
let cell = document.createElement('div');
cell.style.cssText = `
width: 50px;
height: 50px;
`
  cell.classList.add('closed')
  cell.classList.add('cell')
  wrapper.append(cell);  


}
for (let i = 0; i < 64; i++){
let cellSmall = document.createElement('div');
cellSmall.style.cssText = `
width: 25px;
height: 25px;
border: 1px solid black
`
  // cellSmall.classList.add('closed')
  cellSmall.classList.add('cellSmall')
  cellSmall.classList.add('color')
  wrapperSmall.append(cellSmall);  


}
// let cell = document.querySelectorAll('.cell')
// console.log(cell)
//   let num = cell
//   for (let index = 1; index <= 10; index++) {
//     let x = Math.random() * num
//     console.log(x)
//   }





function getRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);

  return `rgb(${r}, ${g}, ${b})`;
}

let colors = document.querySelectorAll('.color')
for (let item of colors) {
 
  const color = getRandomColor()
  item.style.backgroundColor = color
}

wrapperSmall.addEventListener('click', event => {

  let color = event.target.style.backgroundColor
  
  console.log(color)

        wrapper.addEventListener('click', event => {
          event.target.style.backgroundColor = color
          
          // event.target.style.border = '1px solid black'
          
        })
       wrapper.addEventListener('dblclick', event => {
          event.target.style.backgroundColor = 'rgb(201, 197, 197)'
          
          // event.target.style.border = '1px solid black'
        })         
})
