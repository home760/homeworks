// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
// Знайти елемент із id=”optionsList”. Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
// Встановіть в якості контента елемента з класом testParagraph наступний параграф – This is a paragraph
// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.




const byTagName = document.querySelectorAll('p')
byTagName.forEach((element) => { element.style.background = "#ff0000" })
console.log(byTagName)

const byId = document.getElementById('optionsList')
console.log(byId);
console.log(byId.parentElement);
console.log(byId.childNodes) 

const byClass = document.querySelector("#testParagraph")
byClass.innerText = byClass.innerText.replace('Last Chance! Free shipping on all orders ends today.', 'This is a paragraph')
console.log(byClass)

let addClass = document.querySelector(".main-header")
addClass = addClass.children
for (let item of addClass)
{ item = item.classList.add('nav-item') };
console.log(addClass)


let removeClass = document.querySelectorAll(".section-title")
for (let item of removeClass) {
    item = item.classList.remove("section-title")
}
console.log(removeClass)
