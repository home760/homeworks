// Створити поле 30*30 з білих клітинок за допомогою елемента ““`.
// При натисканні на білу клітинку вона повинна змінювати колір на чорний. При натисканні на чорну клітинку вона повинна змінювати колір назад на білий.
// Сама таблиця повинна бути не вставлена у вихідний HTML-код, а згенерована і додана в DOM сторінки за допомогою Javascript.
// Обработчик события click нужно повесить на всю таблицу. События всплывают от элемента вверх по DOM дереву, их все можно ловить с помощью одного обработчика событий на таблице, и в нем определять, на какую из ячеек нажал пользователь.
// При клике на любое место документа вне таблицы, все цвета клеточек должны поменяться на противоположные (подсказка: нужно поставить Event Listener на <body>).
// Чтобы поменять цвета всех клеточек сразу, не нужно обходить их в цикле. Если помечать нажатые клетки определенным классом, то перекрасить их все одновременно можно одним действием – поменяв класс на самой таблице.

let wrapper = document.createElement('div')
wrapper.style.cssText = `
    
     height: 900px;
     width: 900px;
     border: 2px solid black;
     display: flex;
     flex-flow: row wrap
     `  

document.body.append(wrapper)
for (let i = 1; i <= 900; i++) {
  div = document.createElement('div')

div.classList.add('div') 
  wrapper.append(div)  


}

wrapper.addEventListener('click', event => {
   let active = null
  event.target.classList.toggle('active')
  event.stopPropagation()
  active = event.target 
   

   document.body.addEventListener('click', event => {
    wrapper.classList.toggle('active')
    active.classList.toggle('not_active')

  })

})






//    ЧЕРЕЗ ТАБЛИЦІ, ТЕЖ ПРАЦЮЄ



// let table = document.createElement('table');
// table.style.border = '1px solid black'
// table.style.borderSpacing = '0'
// // table.style.background = 'white'
  
// for (let i = 0; i < 30; i++) {
// 	let tr = document.createElement('tr');
//   tr.innerText = ""
//   tr.style.height = '30px'

  

// 	for (let i = 0; i < 30; i++) {
//     let td = document.createElement('td');
//     td.innerText = ""
//     td.style.width = '30px'
//     td.style.border = '1px solid black'

//     //  td.style.boxSizing = 'border-box'
//     tr.appendChild(td);
    
// 	}
// 	table.appendChild(tr);
	
// }
// document.body.append(table)

// table.addEventListener('click', event => {
//   event.target.classList.toggle('active')
//   let active = event.target
//   document.body.addEventListener('click', event => {
//     active.classList.toggle('not_active')
//     table.classList.toggle('active')
//     // active.classList.toggle('active')
//     })
// })