// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.

let numFirst = +prompt('Enter first number');
let numSecond = +prompt('Enter second number');
let operation = prompt('Enter operation');

function calculation(numFirst, numSecond, operation) {
  switch (operation) {
    case '+':
      return numFirst + numSecond;
    
    case '-':
      return numFirst - numSecond;
    
    case '*':
      return numFirst * numSecond;
    
    case '/':
      if (numSecond === 0) {
        return 'Ця операція неможлива';
      }
      return numFirst / numSecond;
    default:
      return 'Error';
    
  } 
  
  }
console.log(calculation(numFirst, numSecond, operation))