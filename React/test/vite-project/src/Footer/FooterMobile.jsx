import React from "react";
import {Link} from 'react-router-dom'
import FooterItem from "./FooterItem";
import Insta from './svg/socialnet/insta.svg?react'
import YouTube from './svg/socialnet/youtube.svg?react'
import TikTok from './svg/socialnet/tiktok.svg?react'
import Facebook from './svg/socialnet/facebook.svg?react'
import Twitter from './svg/socialnet/twitter.svg?react'
import Visa from './svg/payment/visa.svg?react'
import Mastercard from './svg/payment/mastercard.svg?react'
import Applepay from './svg/payment/applepay.svg?react'
import Googlepay from './svg/payment/googlepay.svg?react'
import Logo from './svg/logo.svg?react'
import Home from './svg/menu/home.svg?react'
import Account from './svg/menu/account.svg?react'
import Favorites from './svg/menu/favorites.svg?react'
import Cart from './svg/menu/cart.svg?react'


const Footer = () => {
    return (
        <footer>
            <p>Welcome to our online store bags and backpacks! We have a wide range of quality and stylish bags and backpacks for all your needs and tastes. We offer various models from leading manufacturers from all over the world. We have classic black bags for work, stylish and bright backpacks for travel, sports bags for fitness and more. We always work to ensure that our customers are satisfied with the purchases in our store. We offer fast delivery, convenient payment and quality guarantee for all our products. Thanks to our large number of filters on the site, you can quickly and easily find what you need. Our filters allow you to search for bags and rucksacks by size, material, brand, color and many other parameters. If you have any questions or need help in product selection, our managers are always ready to provide you with qualified assistance and advice. Do not take time to find the perfect bag or backpack. Make your choice now and enjoy shopping in our online store</p>
            <div>
                <FooterItem title='Information'>
                    <li><Link>Contacts</Link></li>
                    <li><Link>Payment & Delivery</Link></li>
                    <li><Link>Returns</Link></li>
                    <li><Link>Guarantee</Link></li>
                    <li><Link>Discount</Link></li>
                    <li><Link>Special proposal</Link></li>
                </FooterItem>
                <FooterItem title='About us'>
                    <li><Link>About brand</Link></li>
                    <li><Link>Our values</Link></li>
                    <li><Link>Partnerships</Link></li>
                    <li><Link>Blog</Link></li>
                    <li><Link>Shops</Link></li>
                </FooterItem>
                <FooterItem title='Follow us'>
                    <li><a href="https://www.instagram.com/modivo_ua/" target="_blank" rel="noopener noreferrer"> <Insta /> </a></li>
                    <li><a href="https://www.youtube.com/channel/UCYGPXwVkOgTUbpmV9uSYH8Q" target="_blank" rel="noopener    noreferrer"><YouTube /></a></li>
                    <li><a href="https://www.tiktok.com/@modivo?lang=en" target="_blank" rel="noopener noreferrer"><TikTok /></a></li>
                    <li><a href="https://www.facebook.com/modivoua" target="_blank" rel="noopener noreferrer"><Facebook /></a></li>
                    <li><a href="https://x.com/modivo_sa" target="_blank" rel="noopener noreferrer"><Twitter/></a></li>
                </FooterItem>
                <FooterItem title='Payment method'>
                    <li><Visa /></li>
                    <li><Mastercard /></li>
                    <li><Applepay /></li>
                    <li><Googlepay/></li>
                </FooterItem>
                <div>
                    <Logo />
                    <p>All rights reserved  © Protect, 2023</p>
                </div>
            </div>
            <div>
                <Link>
                    <Home />
                    <p>Home</p>
                </Link>
                <Link>
                    <Account />
                    <p>Account</p>
                </Link>
                <Link>
                    <Favorites />
                    <p>Favorites</p>
                </Link>
                <Link>
                    <Cart />
                    <p>Cart</p>
                </Link>
            </div>

        </footer>
    )
}
export default Footer