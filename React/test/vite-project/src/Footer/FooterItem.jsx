import React, { useState } from "react";
import ToOpen from './svg/open.svg?react'
import PropTypes from 'prop-types'


const FooterItem = (title, children) => {
    const [isOpenFooterMenu, setOpenFooterMenu] = useState(false)
    const openFooterMenu = !isOpenFooterMenu
    return (
        <>
            <div>
                <h4>{title}</h4>
                <button onClick={openFooterMenu} className={`toopen ${isOpenFooterMenu ? 'open-animated' : ''}`}><ToOpen/></button>
            </div>
            {isOpenFooterMenu && 
                <ul>
                    {children}
                </ul>
            }
        </>
    )
}

FooterItem.defaultProps = {
    isOpenFooterMenu: false
}

FooterItem.PropTypes = {
    title: PropTypes.string,
    openFooterMenu: PropTypes.func
}

export default FooterItem