import Modal from './Modal'
import ModalBody from './ModalBody'
import ModalClose from './ModalClose';
import ModalFooter from './ModalFooter';
import ModalHeader from './ModalHeader';
import ModalWrapper from './ModalWrapper';
import PropTypes from 'prop-types'


const FirstModal = (props) => {
    const {
        isOpen,
        clickFirst,
        title,
        source,
        mainText,
        textFirst,
        clickSecond,
        textSecond,
        handleClose,
        clickOut,
    } = props

    const handleOutside = (event) => {
        if (!event.target.closest('.modal')){
            handleClose()
        }
    }
    return (
        <>
            <ModalWrapper isOpen={isOpen} clickOut={clickOut} handleOutside={handleOutside}>
                <Modal>
                    <ModalHeader text={title} source={source}>
                        <ModalClose click={handleClose} visibleClose={true}/>
                    </ModalHeader>
                    <ModalBody text={mainText} />
                    <ModalFooter
                        clickFirst={clickFirst}
                        textFirst={textFirst}
                        clickSecond={clickSecond}
                        textSecond={textSecond}
                    />
                </Modal>
            </ModalWrapper>
        </>
    )
}
FirstModal.propTypes = {
        isOpen: PropTypes.bool,
        clickFirst: PropTypes.func,
        title: PropTypes.string,
        source: PropTypes.string,
        mainText: PropTypes.string,
        textFirst: PropTypes.string,
        clickSecond: PropTypes.func,
        textSecond: PropTypes.string,
        handleClose: PropTypes.func,
}
export default FirstModal