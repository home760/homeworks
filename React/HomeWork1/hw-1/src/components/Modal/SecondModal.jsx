import Modal from './FirstModal'
import ModalBody from './ModalBody'
import ModalClose from './ModalClose';
import ModalFooter from './ModalFooter';
import ModalHeader from './ModalHeader';
import ModalWrapper from './ModalWrapper';
import pic_two from './tweet.png'



const SecondModal = (props) => {
    const test = () => { alert('All is good') }
    const {
        title = 'Save tweet?',
        visibleClose=true,
        handleClose,
        mainText='You can save this to send later from your unsent Tweets.',
        clickFirst={test},
        textFirst='Save',
        clickSecond={test},
        textSecond='Discard',
        source = { pic_two },
    } = props
        
    const handleOutside =(event) => {
        if (!event.target.closest('.modal')) {
            handleClose()
        }
    }
    return (
        <ModalWrapper handleOutside={handleOutside}>
            <Modal>
                <ModalHeader text={title} source={source}>
                    <ModalClose click={handleClose} visibleClose={visibleClose}/>
                </ModalHeader>
                <ModalBody text={mainText} />
                <ModalFooter
                    clickFirst={clickFirst}
                    textFirst={textFirst}
                    clickSecond={clickSecond}
                    textSecond={textSecond}
                />
            </Modal>
        </ModalWrapper>

    )
}
export default SecondModal