import PropTypes from 'prop-types';
const ModalBody = ({ text }) => {
    return (
        <div className="modal-body">
            <p>
                {text}
            </p>
        </div>
    )
}
ModalBody.protoTypes = {
    text: PropTypes.string
}
export default ModalBody