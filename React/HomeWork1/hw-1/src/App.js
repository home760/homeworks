import { useState } from 'react';
import './App.css';
import Button from './components/Button/Button';
import FirstModal from './components/Modal/FirstModal';
import SecondModal from './components/Modal/FirstModal';
import pic from './components/Modal/kepka.jpg'
import picSecond from './components/Modal/tweet.png'



const App = () =>{
  const test = () => { alert('All is good') }
  const [isModal, setIsModal] = useState(false)
  const [isModalTweet, setIsModalTweet] = useState(false)

  const handleModal = () => {setIsModal(!isModal)}
  const handleModalTweet = () => setIsModalTweet(!isModalTweet)

  return (
    <>
      <div className="App">
          <main className="App-main">
            <div className='buttons-wrapper'>
              <Button colored click={handleModal} children='Open first modal' buttonType='button'/>
              <Button monochrom click={handleModalTweet} children='Open second modal' buttonType='button' />
            <FirstModal
                clickFirst = {test}
                isOpen={isModal}
                title='Product Delete!'
                source={pic}
                mainText='By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.'
                textFirst='NO, CANCEL'
                clickSecond={test}
                textSecond='YES, DELETE'
                handleClose={handleModal}
                />
            <SecondModal
                clickFirst = {test}
                isOpen={isModalTweet}
                title='Save tweet?'
                source={picSecond}
                mainText='You can save this to send later from your unsent Tweets.'
                textFirst='Save'
                clickSecond={test}
                textSecond='Discard'
                handleClose={handleModalTweet}
            />
            </div>
          </main>
    </div>
  </>
  );
  
}
export default App;
