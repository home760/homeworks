import './reset.scss'
import { useState, useEffect } from 'react';
import './App.css';
import Header from './components/Header/components/Header';
import Hero from './components/Hero/Hero';
import Footer from './components/Footer/Footer';
import Products from './components/Products/Products';
import classNames from 'classnames';



const App = () => {
  const data = JSON.parse(localStorage.getItem('favorites') || '[]');
  const basket = JSON.parse(localStorage.getItem('bought') || '[]');
  
  
  const [favorites, setFavorite] = useState([...data])
  const [bought, setBought] = useState([...basket])

  useEffect(() => {
    const handleLocal = () => {
      localStorage.setItem('favorites', JSON.stringify(favorites))
      localStorage.setItem('bought', JSON.stringify(bought))
    }

    handleLocal()
  }, [favorites, bought])

  const hearts = JSON.parse(localStorage.getItem('isActiveHeart') || '[]');
    
  const [isActiveHeart, setActiveHeart] = useState([...hearts])

  useEffect(() => {
      localStorage.setItem('isActiveHeart', JSON.stringify(isActiveHeart))
  }, [isActiveHeart])


  
  const handleBought = (item) => {
    setBought([...bought, item]);
  }

  const handleFavorite = (test, index) => {
    const isAdded = favorites.some((favorite) => favorite.article === test.article)

    if (isAdded) {
      const existFavorites = favorites.filter((favorite) => favorite.article !== test.article);
      const existHearts = isActiveHeart.filter((added) => added !== index)
      setFavorite(existFavorites);
      setActiveHeart(existHearts)
    
    } else {
      setFavorite([...favorites, test]);
      setActiveHeart([...isActiveHeart, index])
    }
    console.log(index)
    console.log(isActiveHeart)
  }

  return (
    <>
        <Header
          favorites={favorites.length}
          toBuy = {bought.length}
          
        />
        <Hero />
      <Products
          handleFavorite={handleFavorite}
          buyClick={handleBought}
          isActiveHeart={isActiveHeart}
        classNames={classNames}
        />
        <Footer />
      </>
    )
 
  }

export default App
