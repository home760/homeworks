import Cards from "./Cards";
import { sendRequest } from './../../helpers/sendRequest';
import { useEffect, useState } from "react";
import './Products.scss'
import FirstModal from "../Modal/FirstModal";


const Products = ({ classNames, handleFavorite, buyClick, isActiveHeart}) => {

    const [products, setProducts] = useState([])
    const [currentProduct, setCurrentProduct] = useState({})
    const [isOpen, setIsOpen] = useState(false)
    const[forBuyCard, setToBuyCard] = useState({})
    const handleCurrentProduct = (cardProduct, index) => {
        setCurrentProduct(cardProduct, index)
        handleFavorite(cardProduct, index)
    }
    const toBuyCurrrent = (buyCard) => {
        setToBuyCard(buyCard)
    }

    const handleModal = () => setIsOpen(!isOpen)

    useEffect(() => {
        sendRequest('/data.json')
            .then((results ) => {
                setProducts(results)
            })
    }, [])

    return (
        <div className="conteiner products-wrapper">

            <Cards
                date={products}
                classNames={classNames}
                handleModal={handleModal}
                toBuyCurrrent={toBuyCurrrent}
                handleCurrentProduct={handleCurrentProduct}
                isActiveHeart={isActiveHeart}
            />
            <FirstModal
                isOpen={isOpen}
                handleClose={handleModal}
                clickFirst={() => {
                    handleModal()
                    buyClick(forBuyCard)
                } }
                title={forBuyCard.name}
                source={forBuyCard.url}
                article={forBuyCard.article}
                price={forBuyCard.price}
                color={forBuyCard.color}
                desc={forBuyCard.desc}
                textFirst='Додати в кошик'
            />
        </div>

    )
}
export default Products
