import { ReactComponent as Facebook } from "./icon/facebook.svg";
import { ReactComponent as Insta } from "./icon/insta.svg";
import { ReactComponent as Tweeter } from "./icon/tweeter.svg";

const Socialnet = () => {
    return (
        <div className="socialnet-wrapper">
            <a href="#"><Facebook /></a>
            <a href="#"><Insta/></a>
            <a href="#"><Tweeter/></a>
        </div>
    )
}
export default Socialnet