import Basket from './Basket'
import Heart from './Heart'
import PropTypes from 'prop-types'

const CustumerButtons = ({favorites, toBuy, children}) => {
    return (
        <div className='custumer-icons'>
            <div className='heart-icon' >
                <Heart><p>{ favorites}</p></Heart>
            </div>
            <div className='heart-icon'>
                <Basket><p>{ toBuy} </p> </Basket>
            </div>
        </div>
    )
}

export default CustumerButtons