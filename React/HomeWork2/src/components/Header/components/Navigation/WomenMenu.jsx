import cx from "classnames"
const WomenMenu = ({classNames, children}) => {
    return (
        <li className={cx("nav-menu-item", classNames)}>
            <a href="#">
                {children}
            </a>
        </li>
    )
}
export default WomenMenu