import Button from '../../components/Button/Button'
import { ReactComponent as Heart } from './heart.svg'
import './Order.scss'
import { useSelector} from 'react-redux/es/hooks/useSelector'
import { selectorIsActiveHeart, selectorBuy} from '../../store/selectors'
import { ReactComponent as Close } from './Close.svg'
import { ReactComponent as Plus } from './plus.svg'
import { ReactComponent as Minus } from './minus.svg'
import { actionAddToBuy, actionMinusFromBuy, actionRemoveFromBuy} from '../../store/appSlice'
import { useDispatch } from 'react-redux';



const OrderItem = ({ handleModal, item, currentRemove, handleCurrentProduct }) => {
    const { article, color, name, price, url, counter } = item 
    const isActiveHeart = useSelector(selectorIsActiveHeart)
    const dispatch = useDispatch()
    const handlePlusItem = () => dispatch(actionAddToBuy(item))
    const handleMinusItem = () => {
        counter>1 ? dispatch(actionMinusFromBuy(item)) : dispatch(actionRemoveFromBuy(item))
    }
    
    return (
        <div className="bought-item conteiner">
            <div className="bought-item-img">
                <img src={url} alt={name} />
            </div>
            <div>
                <p>{name}</p>
                <p>{article}</p>
                <p>{color}</p>
            </div>
            <div className="buttons-wrap">
                <Heart id={article}
                    onClick={() => {
                        handleCurrentProduct(item, item.article)
                    }}
                    className={`heart_icon_ ${isActiveHeart.includes(item.article) ? 'active-heart' : ''} `}
                />
                <div className='counter-wrapper'>
                    <button onClick={handlePlusItem}><Plus/></button>
                    <p>{counter}</p>
                    <button onClick={handleMinusItem}><Minus/></button>
                </div>
                <div>
                    <p>{price}</p>
                </div>
                    <Close className='close' onClick={() => {
                        handleModal(item)
                        currentRemove(item)

                    }
                        } />
            </div>
        </div>
    )
}
OrderItem.defaultProps = {
    isActiveHeart: false
}
export default OrderItem