import * as Yup from "yup"

export const validationSchema = Yup.object({
    name: Yup.string()
        .matches(/[а-яА-ЯёЁa-zA-Z]$/, 'Не може мати числові значення')
        .min(2, 'Занадто коротке імʼя')
        .max(16, 'Занадто довге імʼя')
        .required('Обовʼязкове поле'),

    surname: Yup.string()
        .matches(/[а-яА-ЯёЁa-zA-Z]$/, "Не може мати числові значення")
        .min(2, "Занадто коротке прізвище")
        .max(16, "Занадто довге прізвище")
        .required("Обов'язкове поле"),
    age: Yup.string()
        .matches(/^\d+$/, "Має бути числом")
        .max(2, "Ви динозавр?")
        .required("Обов'язкове поле"),
    adress: Yup.string()
        .min(2, "Занадто коротка адреса")
        .max(25, "Занадто довга адреса")
        .required("Обов'язкове поле"),
    telephone: Yup.string()
        .matches(/^\d+$/, "Має бути числом")
        .min(10, "Номер занадто короткий")
        .max(10, "Номер занадто довгий")
        .required("Обов'язкове поле"),
})