
import FavoriteItem from "./FavoriteItem";
import FirstModal from "../../components/Modal/FirstModal";
import { useState, useEffect } from "react";
import { selectorFavorites } from "../../store/selectors";
import { actionAddToFavorite } from "../../store/appSlice";
import { actionAddToBuy } from "../../store/appSlice";
import { actionIsOpenModal } from "../../store/appSlice";
import { useSelector, useDispatch } from "react-redux";
import { selectorBuy } from "../../store/selectors";
import { selectorIsActiveHeart, selectorIsOpenModal } from '../../store/selectors'



const FavoritePage = () => {
    const dispatch = useDispatch()
    const favorites = useSelector(selectorFavorites)
    const bought = useSelector(selectorBuy)
    const isActiveHeart = useSelector(selectorIsActiveHeart)
    const isOpen = useSelector(selectorIsOpenModal)

    useEffect(() => {
    const handleLocal = () => {
      localStorage.setItem('favorites', JSON.stringify(favorites))
        localStorage.setItem('bought', JSON.stringify(bought))
        localStorage.setItem('isActiveHeart', JSON.stringify(isActiveHeart))
    }

    handleLocal()
    }, [favorites, bought, isActiveHeart])

    const handleModal = () => dispatch(actionIsOpenModal())

    const [currentCard, setCurrentCard] = useState({})   
    const handleCurrentCard = (currentItem) => setCurrentCard(currentItem)
    
    const toBuyCurrrent = (currentCard) => {
        dispatch(actionAddToBuy(currentCard))
    }

    const handleUnFavorites = (currentCard) => dispatch(actionAddToFavorite(currentCard))
    
    const favCard = favorites.map((favorite) => (
        <FavoriteItem
            favorite={favorite}
            key={favorite.article}
            handleUnFavorites={(currentCard) => { 
                handleUnFavorites(currentCard)
               handleCurrentCard(currentCard) 
                
            } }
            handleModal={(currentCard) => { 
                handleModal(currentCard)
               handleCurrentCard(currentCard) 
            } }
            bought={bought}
    
        /> )
)
    return (
        <>
            <div className="favorites-wrapper">
                <h3 className="title">Улюблены товари</h3>
                {favCard.length > 0 ? favCard : <p>Список улюбленого порожній.</p>}
            </div>
             <FirstModal
                isOpen={isOpen}
                    handleClose={handleModal}
                    clickFirst={() => {
                        handleModal()
                        toBuyCurrrent(currentCard)
                    } }
                    title={currentCard.name}
                    source={currentCard.url}
                    article={currentCard.article}
                    price={currentCard.price}
                    color={currentCard.color}
                    desc={currentCard.desc}
                    textFirst='Додати в кошик'
            />

        </>

    )
}

export default FavoritePage