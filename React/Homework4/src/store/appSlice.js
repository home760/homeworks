
import { createSlice } from "@reduxjs/toolkit";
import { sendRequest } from "../helpers/sendRequest";

const appSlice = createSlice({
  name: 'app',
  initialState: {
    favorites: JSON.parse(localStorage.getItem('favorites') || '[]'),
    bought: JSON.parse(localStorage.getItem('bought') || '{}'),
    isActiveHeart: JSON.parse(localStorage.getItem('isActiveHeart') || '[]'),
    productsCards: [],
    isOpen: false,
    formData: {
      name: 'Світлана',
      surname: "Білокінь",
      age: "18",
      adress: "вул.Шевченка 460, Черкаси",
      telephone:"0911122333"
    }
  },
  reducers: {

    
    actionAddToFavorite: (state, { payload }) => {
      
       const isAdded = state.favorites.some((favorite) => favorite.article === payload.article)
      if (isAdded) {
        state.favorites = state.favorites.filter((favorite) => favorite.article !== payload.article);
        state.isActiveHeart = state.isActiveHeart.filter((added) => added !== payload.article)
      } else {
        state.favorites = [...state.favorites, payload];
        state.isActiveHeart = [...state.isActiveHeart, payload.article]
      }

    },

    actionAddToBuy: (state, { payload }) => {
      const isAdded = state.bought.hasOwnProperty(payload.article);
      if (isAdded) {
        state.bought[payload.article].counter += 1;
      } else {
          state.bought = { ...state.bought, [payload.article]: { ...payload, counter: 1 } };
      }
    },
    actionMinusFromBuy: (state, { payload }) => {
        state.bought[payload.article].counter -= 1;
    },

    actionRemoveFromBuy: (state, { payload }) => {
      const { [payload.article]: removedItem, ...newBought } = state.bought;
      state.bought = newBought;
    },

    actionAddToProductsCards: (state, { payload }) => {
      state.productsCards = [...payload]
    }, 

    actionIsOpenModal: (state) => {
      state.isOpen = !state.isOpen
    },

    actionUpdateCv: (state, { payload }) => {
      state.formData = {...payload}
    },

    actionOnSubmit: (state, { payload }) => {
      localStorage.removeItem('bought');
      state.bought = {}
    }
  
  }
  
})

export const { actionAddToFavorite } = appSlice.actions
export const {actionAddToBuy} = appSlice.actions
export const { actionRemoveFromBuy } = appSlice.actions
export const { actionAddToProductsCards } = appSlice.actions
export const { actionIsOpenModal } = appSlice.actions
export const { actionUpdateCv } = appSlice.actions 
export const {actionOnSubmit} = appSlice.actions
export const {actionMinusFromBuy} = appSlice.actions

export const actionFetchProducts = () => (dispatch) => {
  return sendRequest('/data.json')
    .then((results) => {
       dispatch(actionAddToProductsCards(results))
    })
}


export default appSlice.reducer