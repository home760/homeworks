import './Modal.scss'
import PropTypes from 'prop-types'
const ModalWrapper = ({ children, isOpen, handleOutside }) => {
    return (
        <>
            {isOpen && (<div className="modal-wrapper" onClick={ (event => handleOutside(event))}>{children} </div>)    }
        </>
    )
}
ModalWrapper.defaultProps = {
    isOpen: false
}

ModalWrapper.propTypes = {
    children: PropTypes.any,
    isOpen: PropTypes.bool
}

export default ModalWrapper