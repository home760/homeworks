import Cards from "./Cards";
import { sendRequest } from './../../helpers/sendRequest';
import { useEffect, useState } from "react";
import './Products.scss'
import FirstModal from "../Modal/FirstModal";
import Hero from "../Hero/Hero";
import { actionAddToBuy, actionAddToFavorite, actionFetchProducts, actionIsOpenModal } from "../../store/appSlice";
import { useDispatch, useSelector } from 'react-redux';
import { selectorFavorites, selectorBuy, selectorIsOpenModal } from "../../store/selectors";
import { selectorIsActiveHeart, selectorProductsCards } from "../../store/selectors";


const Products = () => {
    const data = JSON.parse(localStorage.getItem('favorites') || '[]');
  const basket = JSON.parse(localStorage.getItem('bought') || '{}');
    
    
    const dispatch = useDispatch()
    const [currentProduct, setCurrentProduct] = useState({})
    const [forBuyCard, setToBuyCard] = useState({})
    const favorites = useSelector(selectorFavorites)
    const bought = useSelector(selectorBuy)
    const isActiveHeart = useSelector(selectorIsActiveHeart)
    const productsCards =  useSelector(selectorProductsCards)
    const isOpen =  useSelector(selectorIsOpenModal)


        
    useEffect(() => {
            dispatch(actionFetchProducts())
        }, [dispatch])



    useEffect(() => {
        const handleLocal = () => {
            localStorage.setItem('favorites', JSON.stringify(favorites))
            localStorage.setItem('bought', JSON.stringify(bought))
            localStorage.setItem('isActiveHeart', JSON.stringify(isActiveHeart))
        }
        
        handleLocal()
    }, [favorites, bought, isActiveHeart])
   
    
    const handleCurrentProduct = (cardProduct) => {
        setCurrentProduct(cardProduct)
        
    }
    const handleFavorite = (currentProduct) => {
        dispatch(actionAddToFavorite(currentProduct))
    }
    
    const handleBought = (forBuyCard) => {
        dispatch(actionAddToBuy(forBuyCard))
    }
    
    const toBuyCurrrent = (buyCard) => {
        setToBuyCard(buyCard)
    }
    const handleModal = () => dispatch(actionIsOpenModal())

    return (
        <>
            <main className="main">

                <Hero/>
                <div className="conteiner products-wrapper">

                    <Cards
                        date={productsCards}
                        handleModal={handleModal}
                        toBuyCurrrent={toBuyCurrrent}
                        handleCurrentProduct={(currentProduct) => {
                            handleCurrentProduct()
                            handleFavorite(currentProduct)
                            }
                        }
                        isActiveHeart={isActiveHeart}
                        bought={bought}
                    />
                    <FirstModal
                        isOpen={isOpen}
                        handleClose={handleModal}
                        clickFirst={() => {
                            handleModal()
                            handleBought(forBuyCard)
                        } }
                        title={forBuyCard.name}
                        source={forBuyCard.url}
                        article={forBuyCard.article}
                        price={forBuyCard.price}
                        color={forBuyCard.color}
                        desc={forBuyCard.desc}
                        textFirst='Додати в кошик'
                    />
                </div>
            </main>
        </>

    )
}
export default Products
