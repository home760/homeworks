import PropTypes from 'prop-types'
import cn from 'classnames'
import { Field, ErrorMessage } from 'formik';

const TextAreaBox = (props) => {
    const {
        className,
        label,
        name,
        rows,
        placeholder,
        error,
        ...restProps
    } = props
    return (
        <label className={cn('fom-item', className, { 'has-validation': error })}>
            <p>{label}</p>
            <Field as='textarea' name={name} placeholder={placeholder} {...restProps} />
            <ErrorMessage name={name} component='p' />

        </label>
    )
}

TextAreaBox.defaultProps = {
    rows: 3
}
TextAreaBox.propTypes = {
    className: PropTypes.string,
	label: PropTypes.string.isRequired,
	type: PropTypes.string,
	name: PropTypes.string.isRequired,
	placeholder: PropTypes.string,
    error: PropTypes.object,
    rows:PropTypes.number

}
export default TextAreaBox