import './Button.scss'
import cx from 'classnames'
import PropTypes from 'prop-types'
const Button = ({ click, children, classNames, buttonType, colored, monochrom}) => {

    return (
        <button className={cx('button', classNames, {'_purple':colored}, {'_white':monochrom})} type = {buttonType} onClick={click} >{children}</button>
    )
}

Button.defaultProps = {
    buttonType: 'button',
}
Button.propTypes = {
    click: PropTypes.func,
    classNames: PropTypes.string,
    buttonType: PropTypes.string,
    colored: PropTypes.bool,
    monochrom: PropTypes.bool,
}

export default Button