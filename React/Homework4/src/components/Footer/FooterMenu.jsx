
const FooterMenu = ({title, children}) => {
    return (
        <>
            <div className="footer-item-wrapper">
                <h4 className="footer-title">{title}</h4>
                <ul className="footer-list">
                        {children}
                </ul>
            </div>
        </>
    )
}
export default FooterMenu