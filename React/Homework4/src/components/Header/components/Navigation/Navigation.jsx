import { Link } from "react-router-dom"
import CombosMenu from "./CombosMenu"
import JoggersMenu from "./JoggersMenu"
import MenMenu from "./MenMenu"
import ShopMenu from "./ShopMenu"
import WomenMenu from "./WomenMenu"

const Navigation = () => {
    return (
        <nav>
            <ul className="nav-menu">
            <ShopMenu>Хлопчики</ShopMenu>
            <MenMenu>Дівчата</MenMenu>
            <WomenMenu link="/"> Різдво </WomenMenu>
            <CombosMenu>Чоловіки</CombosMenu>
            <JoggersMenu>Жінки</JoggersMenu>
            </ul>
        </nav>
    )
}
export default Navigation