import cx from "classnames"
const JoggersMenu = ({classNames, children}) => {
    return (
        <li className={cx("nav-menu-item", classNames)}>
            <a href="#">
                {children}
            </a>
        </li>
    )
}
export default JoggersMenu