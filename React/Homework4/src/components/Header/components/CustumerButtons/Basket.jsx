import {ReactComponent as Shopping} from './shopping.svg'


const Basket = ({children}) => {
    return (
        <>
        <Shopping/>
        <div> {children}  </div>
        </>
    )
}

export default Basket