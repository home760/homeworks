import { Link } from "react-router-dom"
import CustumerButtons from "./CustumerButtons/CustumerButtons"
import Logo from "./Logo/Logo"
import Navigation from "./Navigation/Navigation"
import './Header.scss'
import { ReactComponent as Logoicon } from "./Logo/logo.svg"
import { selectorFavorites, selectorBuy } from "../../../store/selectors"
import { useSelector } from 'react-redux';

const Header = ({ toBuy }) => {
    const favorites = useSelector(selectorFavorites)
    const bought = useSelector(selectorBuy)
    return (
        <header className="header">
            <div className="conteiner">
                <div className="header-wrapper">
                <Logo>
                    <Link to="/" >
                        <Logoicon/>
                    </Link>
                </Logo>
                    <CustumerButtons
                        favorites={favorites.length}
                        toBuy={Object.keys(bought).length}
                    />
                        
              
                </div>
                
                <Navigation />
                
            </div>
            
            </header>
    )
}
export default Header