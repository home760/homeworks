import Cards from "./Cards";
import { useEffect, useState } from "react";
import './Products.scss'
import FirstModal from "../Modal/FirstModal";
import Hero from "../Hero/Hero";
import { actionAddToBuy, actionAddToFavorite, actionFetchProducts, actionIsOpenModal, actionIsListOrTable } from "../../store/appSlice";
import { useDispatch, useSelector } from 'react-redux';
import { selectorFavorites, selectorBuy, selectorIsOpenModal } from "../../store/selectors";
import { selectorIsActiveHeart, selectorProductsCards, selectorIsListOrTable } from "../../store/selectors";
import { ReactComponent as List } from './list.svg'
import { ReactComponent as Table } from './table.svg'
import Button from "../Button/Button";


const Products = () => {
    // eslint-disable-next-line
    const data = JSON.parse(localStorage.getItem('favorites') || '[]');
    // eslint-disable-next-line
  const basket = JSON.parse(localStorage.getItem('bought') || '{}');
    
    
    const dispatch = useDispatch()
    // eslint-disable-next-line
    const [currentProduct, setCurrentProduct] = useState({})
    const [forBuyCard, setToBuyCard] = useState({})
    const favorites = useSelector(selectorFavorites)
    const bought = useSelector(selectorBuy)
    const isActiveHeart = useSelector(selectorIsActiveHeart)
    const productsCards =  useSelector(selectorProductsCards)
    const isOpen =  useSelector(selectorIsOpenModal)


        
    useEffect(() => {
            dispatch(actionFetchProducts())
        }, [dispatch])



    useEffect(() => {
        const handleLocal = () => {
            localStorage.setItem('favorites', JSON.stringify(favorites))
            localStorage.setItem('bought', JSON.stringify(bought))
            localStorage.setItem('isActiveHeart', JSON.stringify(isActiveHeart))
        }
        
        handleLocal()
    }, [favorites, bought, isActiveHeart])
   
    
    const handleCurrentProduct = (cardProduct) => {
        setCurrentProduct(cardProduct)
        
    }
    const handleFavorite = (currentProduct) => {
        dispatch(actionAddToFavorite(currentProduct))
    }
    
    const handleBought = (forBuyCard) => {
        dispatch(actionAddToBuy(forBuyCard))
    }
    
    const toBuyCurrrent = (buyCard) => {
        setToBuyCard(buyCard)
    }
    const handleModal = () => dispatch(actionIsOpenModal())

    const isListOrTable = useSelector(selectorIsListOrTable)
    const handelListOrTable = () => dispatch(actionIsListOrTable(!isListOrTable))

    return (
        <>
            <main className="main">
                <Hero />
                <div className="conteiner table-list-button">
                    <Button click={handelListOrTable} className="card-list-button">
                        {isListOrTable ? <List /> : <Table />}
                    </Button>

                </div>
                <div className={`container ${isListOrTable ? 'products-wrapper-table' : 'products-wrapper-list'}`}>

                    <Cards
                        date={productsCards}
                        handleModal={handleModal}
                        toBuyCurrrent={toBuyCurrrent}
                        handleCurrentProduct={(currentProduct) => {
                            handleCurrentProduct()
                            handleFavorite(currentProduct)
                            }
                        }
                        isActiveHeart={isActiveHeart}
                        bought={bought}
                    />
                    <FirstModal
                        isOpen={isOpen}
                        handleClose={handleModal}
                        clickFirst={() => {
                            handleModal()
                            handleBought(forBuyCard)
                        } }
                        title={forBuyCard.name}
                        source={forBuyCard.url}
                        article={forBuyCard.article}
                        price={forBuyCard.price}
                        color={forBuyCard.color}
                        desc={forBuyCard.desc}
                        textFirst='Додати в кошик'
                    />
                </div>
            </main>
        </>

    )
}
export default Products
