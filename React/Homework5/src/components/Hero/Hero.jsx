import Button from "../Button/Button"
import Title from "./Title"
import './Hero.scss'

const Hero = () => {
    return (
        <div className="hero">

            <Title>
                <div className="hero-img-wrapper">
                    <a href="#"><img className="hero-img" src="https://www.next.ua/nxtcms/resource/blob/5793908/bbde78ddde277945bc75a629a8aad8b5/shop-all-christmas-data.jpg" alt="watch all products" /></a>
                    <a href="#"><img className="hero-img" src="https://www.next.ua/nxtcms/resource/blob/5793900/d9cd9a1fe5ca7118f21f97419432cd55/christmas-decorations-data.jpg" alt="watch all products" /></a>
                    <a href="#"><img className="hero-img" src="https://www.next.ua/nxtcms/resource/blob/5793910/81042069dcb1ebfa136dce4e53cd7311/shop-all-gifting-data.jpg" alt="watch all products" /></a>
                    <a href="#"><img className="hero-img"  src="https://www.next.ua/nxtcms/resource/blob/5793922/053480f2f47d2b4ffe5d563f7eb5a359/shop-all-christmas-clothing-data.jpg" alt="watch all products" /></a>

                </div>
                <h3 className="hero-title">РІЗДВО</h3>
                <p className="hero-text">Час напередодні Різдва так само приносить веселощі, як і великий день. Розкладіть подарунки, прикрасьте кімнати та підготуйте піжами в одному стилі.</p>
            </Title>
        </div>
        )
}
export default Hero