import React from "react";
import Button from "./Button";
import { render, screen, fireEvent } from "@testing-library/react";

const handelClick = jest.fn();
describe("test button", () => {
  test("isButton", () => {
    render(<Button>text</Button>);
    expect(screen.getByText("text")).toBeInTheDocument();
  });
  test("isClassName", () => {
    render(<Button className={"button"}>text</Button>);
    expect(screen.getByRole("button")).toHaveClass("button");
  });
  test("isType", () => {
    render(<Button>text</Button>);
    expect(screen.getByText("text")).toHaveAttribute("type", "button");
  });

  test("isCheng", () => {
    render(<Button type={"submit"}>text</Button>);
    expect(screen.getByRole("button")).toHaveAttribute("type", "button");
  });

  test("isLink", () => {
    render(<Button href={"/link"}>text</Button>);
    expect(screen.getByRole("button")).not.toHaveAttribute("href", "/link");
  });

    test("onClick", () => {
        render(<Button click={handelClick}>text</Button>);
        const button = screen.getByRole("button");
        fireEvent.click(button);
        expect(handelClick).toHaveBeenCalled();
    });
});
