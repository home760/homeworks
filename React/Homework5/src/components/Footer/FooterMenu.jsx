import PropTypes from 'prop-types'
const FooterMenu = ({title, children}) => {
    return (
        <>
            <div className="footer-item-wrapper">
                <h4 className="footer-title">{title}</h4>
                <ul className="footer-list">
                        {children}
                </ul>
            </div>
        </>
    )
}

FooterMenu.propTypes = {
    title: PropTypes.string,
    children: PropTypes.any
}

export default FooterMenu