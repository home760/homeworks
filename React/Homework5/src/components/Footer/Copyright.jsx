import React from 'react'

const Copyright = ({ children }) => {
    return (
        <p className="copyright">{ children}</p>
    )
}
export default Copyright