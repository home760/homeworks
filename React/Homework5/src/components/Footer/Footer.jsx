import './Footer.scss'
import FooterMenu from './FooterMenu'
import Socialnet from './Socialnet'
import Copyright from './Copyright'
import React from 'react'


const Footer = () => {

    return (
        <div className='footer'>
            <div className='conteiner'>

                <div className='footer-menu conteiner'>

                    <FooterMenu title='Need Help'>
                    
                        <a href="#"><li>Contact Us</li></a>
                        <a href="#"><li>Track Order</li></a>
                        <a href="#"><li>Returns & Refunds</li></a>
                        <a href="#"><li>FAQ's</li></a>
                        <a href="#"><li>Career</li></a>
                        
                    </FooterMenu>
                    <FooterMenu title='Company'>
                    
                        <li>About Us</li>
                        <li>euphoria Blog</li>
                        <li>euphoriastan</li>
                        <li>Collaboration</li>
                        <li>Media</li>
                        
                    </FooterMenu>
                    <FooterMenu title='More Info'>
                    
                        <li>Term and Conditions</li>
                        <li>Privacy Policy</li>
                        <li>Shipping Policy</li>
                        <li>Sitemap</li>
                        
                    </FooterMenu>
                    <FooterMenu title='Location'>
                    
                        <li>support@euphoria.in</li>
                        <li>Eklingpura Chouraha, Ahmedabad Main Road</li>
                        <li>(NH 8- Near Mahadev Hotel) Udaipur, India- 313002</li>
                        
                    </FooterMenu>
                </div>
                <Socialnet />
                <Copyright>Copyright © 2023 Euphoria Folks Pvt Ltd. All rights reserved.</Copyright>
            </div>
        </div>
        )
}

export default Footer