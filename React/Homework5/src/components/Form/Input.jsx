import { Field, ErrorMessage } from "formik";
import PropTypes from 'prop-types';
import cn from 'classnames'
import './Form.scss'

const InputBox = (props) => {
    const {
        label,
        className,
        type,
        name,
        placeholder,
        error,
        ...restProps
    } = props
    return (
        <label className={cn('form-item', className, { 'has-validation': error })}>
            <p  className="form-label">{label}</p>
            <Field className="form-control" type={type} name={name} placeholder={placeholder} {...restProps} />
            <ErrorMessage className="error-message" name={name} component='p' />

        </label>
    )
}

InputBox.defaultProps = {
    type: 'text'
}
InputBox.propTypes = {
    className: PropTypes.string,
	label: PropTypes.string.isRequired,
	type: PropTypes.string,
	name: PropTypes.string.isRequired,
	placeholder: PropTypes.string,
	// error: PropTypes.object

}
export default InputBox