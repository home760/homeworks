import React from "react";
import { fireEvent, getByText, render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import FirstModal from './FirstModal';

const handelClick = jest.fn();
describe("test Modal", () => {
  test("render Modal", () => {
    const mockStore = configureStore();
    const initialState = {
      favorites: [],
      bought: {},
      productsCards: [],
      isOpen: false,
    };
    const data = {
        title: "jwnef",
        source: "/",
        article: "wefw",
        textFirst: "wkjebf",
    };
    const store = mockStore(initialState);
    const { asFragment } = render(
      <Provider store={store}>
        <BrowserRouter>
          <FirstModal data={data} />
        </BrowserRouter>
      </Provider>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
