import React from "react";
import Modal from './Modal'
import ModalBody from './ModalBody'
import ModalClose from './ModalClose';
import ModalFooter from './ModalFooter';
import ModalHeader from './ModalHeader';
import ModalWrapper from './ModalWrapper';
import PropTypes from 'prop-types'



const FirstModal = (props) => {
    const {
        isOpen,
        clickFirst,
        title,
        source,
        article,
        textFirst,
        handleClose,
        color,
        price,
        desc
    } = props

    const handleOutside = (event) => {
        if (!event.target.closest('.modal')){
            handleClose()
        }
    }
    return (
        <>
            <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
                <Modal>
                    <ModalHeader text={title} source={source}>
                        <ModalClose click={handleClose} visibleClose={true}/>
                    </ModalHeader>
                    <ModalBody
                        article={article}
                        price={price}
                        color={color}
                        desc={desc}
                    
                    />
                    <ModalFooter
                        clickFirst={clickFirst}
                        textFirst={textFirst}
                    />
                </Modal>
            </ModalWrapper>
        </>
    )
}
FirstModal.propTypes = {
        isOpen: PropTypes.bool,
        clickFirst: PropTypes.func,
        title: PropTypes.string,
        source: PropTypes.string,
        mainText: PropTypes.string,
        textFirst: PropTypes.string,
        clickSecond: PropTypes.func,
        textSecond: PropTypes.string,
        handleClose: PropTypes.func,
}
export default FirstModal