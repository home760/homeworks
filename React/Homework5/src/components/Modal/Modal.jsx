import cx from "classnames"
import PropTypes from 'prop-types'

const Modal = ({ classNames, children, clickStop }) => {
    
    return (
        <div onClick={clickStop} className={cx('modal', classNames)}>
            <div className={cx('modal-box', classNames)}>
                {children}
            </div>

        </div>
    )
}
Modal.propTypes = {
    classNames: PropTypes.string
}
export default Modal