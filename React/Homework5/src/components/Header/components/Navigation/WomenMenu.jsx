import cx from "classnames"
import PropTypes from "prop-types"
import { Link } from "react-router-dom"

const WomenMenu = ({ classNames, children, link }) => {
    return (
        <li className={cx("nav-menu-item", classNames)}>
            <Link to={link}>
                {children}
            </Link>
        </li>
    )
}
WomenMenu.defaultProps = {
    link: "#"
}
export default WomenMenu