import { ReactComponent as Fovorite } from './heart.svg'


const Heart = ({children}) => {
    return (
        <>
        <Fovorite/>
        <div> {children}  </div>
        </>
    )
}

export default Heart