import Basket from './Basket'
import Heart from './Heart'
import { Link } from 'react-router-dom'

const CustumerButtons = ({favorites, toBuy, children}) => {
    return (
        <div className='custumer-icons'>
            <Link to="favorites">
                <div className='heart-icon' >
                    <Heart><p>{ favorites}</p></Heart>
                </div>
            </Link>
            <Link to="order">
            <div className='heart-icon'>
                <Basket><p>{ toBuy} </p> </Basket>
            </div>
            </Link>
        </div>
    )
}

export default CustumerButtons