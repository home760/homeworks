import Button from '../../components/Button/Button'
import './Favorite.scss'
import { ReactComponent as Heart } from './heart.svg'
import { selectorIsActiveHeart } from '../../store/selectors'
import { useSelector } from 'react-redux/es/hooks/useSelector'

const FavoriteItem = ({ handleUnFavorites, favorite, handleModal, bought}) => {
    const { article, color, name, price, url } = favorite 
    // const added = bought.some((boughtItem) => boughtItem.article === favorite.article)
      const added = bought.hasOwnProperty(favorite.article);

    const isActiveHeart = useSelector(selectorIsActiveHeart)
    return (
        <div className="favorite-item conteiner">
            <div className="favorite-item-img">
                <img src={url} alt={name} />
            </div>
            <div className="info">
                <p>{name}</p>
                <p>{article}</p>
                <p>{color}</p>
            </div>
            <div>

                <Button colored click={() => {
                    handleModal(favorite)
                }} >{added === true ? 'Додати ще...' : 'Додати в кошик'}</Button>
            </div>
            <div>
                 <Heart id={article}
                    onClick={() => {
                        handleUnFavorites(favorite, article)
                    }}
                    className={`heart_icon_ ${isActiveHeart.includes(favorite.article) ? 'active-heart' : ''} `}
                />
            </div>
                <p>{price}</p>
        </div>
    )
}
export default FavoriteItem