import { Link, useNavigate, useLocation } from "react-router-dom";
import Button from "../../components/Button/Button";
import "./ErrorPage.scss"

const ErrorPage = () => {
    const location = useLocation()
    const navigate = useNavigate()

    const goHome = () => navigate('/')
    const goBack = () => navigate(-1)
    return (
        <>
            <div className="error-page">
                <div className="img">
                    <img src="https://www.next.ua/images/structural/errors/warning_large.gif" alt="error" />
                </div>
                <div className="error_info">
                    <h2>Uh oh, that page no longer exists... </h2>
                    <p>We're very sorry, the page you have requested could not be found or the URL has been typed incorrectly.</p>
                    <div className="buttons">
                        <Button colored click={goHome} >Back to Homepage</Button>
                        <Button colored click={goBack} > Back to the previous page</Button>
                    </div>
                </div>
            </div>
        </>
    )
}
export default ErrorPage