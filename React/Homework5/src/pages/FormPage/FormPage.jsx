import InputBox from "../../components/Form/Input"
import TextAreaBox from "../../components/Form/TextArea"
import Button from "../../components/Button/Button"
import {useNavigate} from "react-router-dom";
import { Formik, Form } from "formik"
import { useDispatch, useSelector } from "react-redux"
import { selectorFormData, selectorBuy } from '../../store/selectors.js'
import { actionUpdateCv, actionOnSubmit } from '../../store/appSlice.js'
import {validationSchema} from "./validation.js"
import './FormPage.scss'



const FormPage = () => {
    const formDate = useSelector(selectorFormData)
    const bought = useSelector(selectorBuy)
    console.log(bought)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const handleSubmit = () => dispatch(actionOnSubmit())
    return (
        <div className="page__dashboard">
            <div  className="page">
                <Formik
                    initialValues={formDate}
                    validationSchema={validationSchema}
                    onSubmit={(values, { resetForm }) => {
                    dispatch(actionUpdateCv(values))
                        console.log(values)
                        resetForm()
                        navigate('/order/form/success') 
                        
                }}>
                    {({ errors, touched }) => (
                        <Form>
                            <fieldset  className="form-block">
                                <legend>Адреса доставки</legend>
                                <div  className="row">
                                    <div  className="col">
                                        <InputBox
                                            label="Name"
                                            name="name"
                                            placeholder="name"
                                            error={errors.name && touched.name}
                                        />
                                        <InputBox
                                            label="Surname"
                                            name="surname"
                                            placeholder="surname"
                                            error={errors.surname && touched.surname}
                                        />
                                        <InputBox
                                            label="Age"
                                            name="age"
                                            placeholder="age"
                                            error={errors.age && touched.age}
                                        />
                                        <InputBox
                                            label="Adress"
                                            name="adress"
                                            placeholder="adress"
                                            error={errors.adress && touched.adress}
                                        />
                                        <InputBox
                                            label="Telephone"
                                            name="telephone"
                                            placeholder="telephone"
                                            error={errors.telephone && touched.telephone}
                                        />
                                    </div>
                                </div>
                                <Button
                                    // disabled='true'
                                    classNames="submit-button"
                                    colored
                                    buttonType="submit"
                                    click={handleSubmit}
                                >
                                    Перейти до оплати
                                </Button>
                            </fieldset>

                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    )
}
export default FormPage