
import { Link, useNavigate, useLocation } from "react-router-dom";
import Button from "../../components/Button/Button";
import { ReactComponent as Done } from "./done.svg"

// import "./ErrorPage.scss"

const AfterOrderPage = () => {
    const location = useLocation()
    const navigate = useNavigate()

    const goHome = () => navigate('/')
    return (
        <>
            <div className="error-page">
                <div className="img">
                    <Done/>
                </div>
                <div className="error_info">
                    <h2>Ваше замовлення прийнято</h2>
                    <p>Наш оператор зв'яжеться з Вами найближчим часом. Щиро дякуємо за довіру.</p>
                    <div className="buttons">
                        <Button colored click={goHome} >Back to Homepage</Button>
                    </div>
                </div>
            </div>
        </>
    )
}
export default AfterOrderPage