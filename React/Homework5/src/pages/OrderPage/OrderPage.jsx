import OrderItem from "./OrderItem"
import './Order.scss'
import FirstModal from "../../components/Modal/FirstModal"
import { useState, useEffect } from "react"
import { Formik } from "formik"
import { useDispatch, useSelector } from "react-redux"
import { selectorBuy } from "../../store/selectors"
import { selectorFavorites } from "../../store/selectors"
import { actionAddToFavorite } from "../../store/appSlice"
import { actionRemoveFromBuy } from "../../store/appSlice"
import { actionIsOpenModal } from "../../store/appSlice";
import { selectorIsActiveHeart, selectorIsOpenModal } from '../../store/selectors'
import Button from "../../components/Button/Button"
import { Link } from 'react-router-dom';

const OrderPage = () => {
    const dispatch = useDispatch()
    const isActiveHeart = useSelector(selectorIsActiveHeart)
    const bought = useSelector(selectorBuy)
    const favorites = useSelector(selectorFavorites)
    const isOpen = useSelector(selectorIsOpenModal)
   

    const [removeItem, setRemoveItem] = useState({})

    const currentRemove = (removeCard) => {
        setRemoveItem(removeCard)
    }

    useEffect(() => {
        const handleLocal = () => {
            localStorage.setItem('favorites', JSON.stringify(favorites))
            localStorage.setItem('bought', JSON.stringify(bought))
            localStorage.setItem('isActiveHeart', JSON.stringify(isActiveHeart))
        }
        handleLocal()
    }, [favorites, bought, isActiveHeart])


    const [currentProduct, setCurrentProduct] = useState({})

    const handleModal = () => dispatch(actionIsOpenModal())


    const handleCurrentProduct = (cardProduct, index) => {
            setCurrentProduct(cardProduct, index)
            dispatch(actionAddToFavorite (cardProduct))
        }

    const handleRemoveBought = (removeItem) => dispatch(actionRemoveFromBuy(removeItem))

    const order = {};
    let summ = 0;
    Object.values(bought).forEach((item) => {
        order[item.article] = (
            <OrderItem
            item={item}
            key={item.article}
            currentRemove={currentRemove}
            handleModal={handleModal}
            handleCurrentProduct={handleCurrentProduct}
            />
        );
        summ += parseFloat(item.counter)*parseFloat(item.price)
    });

    return (
        <>
            <div className="bought-wrapper">
                <h3 className="title">Кошик</h3>
                {Object.keys(order).length > 0 && (
                    <>
                    {Object.values(order)}
                    <p className="summ">Сума до сплати {summ} ₴ </p>
                    <Button classNames='order-button' colored>
                        <Link to="form" >
                            Оформити замовлення
                        </Link>
                    </Button>
                    </>
                
                    
                )}
                {Object.keys(order).length === 0 && (<p>Кошик порожній.</p>)}

                
            </div>


            <FirstModal
                isOpen={isOpen}
                handleClose={handleModal}
                    clickFirst={() => {
                        handleModal()
                        handleRemoveBought(removeItem)
                    } }
                    title={removeItem.name}
                    source={removeItem.url}
                    article={removeItem.article}
                    price={removeItem.price}
                    color={removeItem.color}
                    desc={removeItem.desc}
                    textFirst='Видалити з кошика'
            />
        </>
    )
}
export default OrderPage