import { Outlet } from "react-router-dom";

const LayoutPage = () => {
    return (
        <div className="main" >
            <Outlet/>
        </div>
    )
}
export default LayoutPage