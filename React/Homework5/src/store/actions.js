import { createAction } from "@reduxjs/toolkit";

export const actionAddToFavorite = createAction('ACTION_ADD_TO_FAVORITE')