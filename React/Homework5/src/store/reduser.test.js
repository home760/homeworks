import React from "react";
import * as actions from './actions'
import { createSlice } from '@reduxjs/toolkit';
import appReducer, {
    
    actionAddToFavorite,
    actionAddToBuy,
    actionRemoveFromBuy,
    actionAddToProductsCards,
    actionIsOpenModal,
    actionUpdateCv,
    actionOnSubmit,
    actionMinusFromBuy,
    actionIsListOrTable
} from './appSlice';

describe("app reducer", () => {
    test('actionAddToBuy', () => {
        const initialState = {
            bought: {},
        }
        const previousState = { ...initialState }
        expect(appReducer(previousState, actionAddToBuy({card: 1}))).toEqual({
            ...initialState,
                bought: {
                    undefined: {
                    card: 1,
                    counter: 1,
                    },
                },
        })
    })
    test("isOpen", () => {
        const initialState = {
        isOpen: false,
        };
        const previsionState = initialState;
        expect(appReducer(previsionState, actionIsOpenModal(true))).toEqual({
        isOpen: true,
        });
    });

    test('addToFavorite', () => {
        const initialState = {
            favorites: [],
            isActiveHeart: [],
        };

        const cardToAdd = { card: 1 };
        const previousState = { ...initialState };

        const newState = appReducer(previousState, actionAddToFavorite(cardToAdd));

        const expectedIsActiveHeart = newState.isActiveHeart.filter((item) => item !== undefined);

        expect(newState).toEqual({
            ...initialState,
            favorites: [cardToAdd],
            isActiveHeart: expectedIsActiveHeart,
        });
    });

})