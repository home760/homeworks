import {configureStore} from '@reduxjs/toolkit'
// import thunk from 'redux-thunk';
// import logger from "redux-logger";
import appReducer from "./appSlice"

export default configureStore({
	reducer: {
		app: appReducer,
	}
});
