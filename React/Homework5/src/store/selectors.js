export const selectorFavorites = (store) => store.app.favorites;
export const selectorBuy = (store) => store.app.bought;
export const selectorIsActiveHeart = (store) => store.app.isActiveHeart;
export const selectorProductsCards = (store) => store.app.productsCards
export const selectorIsOpenModal = (store) => store.app.isOpen
export const selectorFormData = (store) => store.app.formData
export const selectorIsListOrTable = (store) => store.app.isListOrTable