import { Route, Routes } from 'react-router-dom';
import './reset.scss'
import './App.css';
import Header from './components/Header/components/Header';
import Footer from './components/Footer/Footer';
import Products from './components/Products/Products';
import ErrorPage from './pages/Error/ErrorPage';
import FavoritesPage from './pages/FavoritesPage/FavoritesPage';
import LayoutPage from './pages/Layout/LayoutPage';
import OrderPage from './pages/OrderPage/OrderPage';
import FormPage from './pages/FormPage/FormPage';
import AfterOrderPage from './pages/AfterOrderPage/AfterOrderPage';




const App = () => {

  return (
    <>
      <div className="wrapper">
      <Header/>
        <Routes>
          <Route element={<LayoutPage/>}>
            <Route path="/" element={<Products/>} />
            <Route path="order" element={<OrderPage/>} />
            <Route path="favorites" element={<FavoritesPage />} />
            <Route path="/order/form" element={<FormPage />} />
            <Route path="/order/form/success" element={<AfterOrderPage />}  />
            <Route path="*" element={<ErrorPage/>} />
          </Route>
      </Routes>
      <Footer />
      </div>
    </>
    )
 
  }
export default App
