import CardItem from './CardItem';

const Cards = ({
    date,
    classNames,
    handleModal,
    handleCurrentProduct,
    productClick,
    toBuyCurrrent,
    isActiveHeart, 
    counter,
    bought
}) => {
    const cardItem = date.map((item, index) => ( 
        <CardItem
            index={index}
            handleModal={handleModal}
            handleCurrentProduct={handleCurrentProduct}
            toBuyCurrrent={toBuyCurrrent}
            isActiveHeart={isActiveHeart}
            classNames={classNames}
            item={item}
            key={item.article}
            productClick={productClick}
            counter={counter}
            bought={bought}

        />
    ))
    return (
        cardItem
    )
}
export default Cards