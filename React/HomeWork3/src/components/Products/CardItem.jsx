import Button from "../Button/Button"
import { ReactComponent as Heart } from '../Header/components/CustumerButtons/heart.svg'
import PropTypes from 'prop-types'

const CardItem = ({
    item,
    handleModal,
    handleCurrentProduct,
    toBuyCurrrent,
    isActiveHeart,
    counter,
    bought
}) => {
    const { name, url, price, color, article } = item
    const added = bought.some((boughtItem) => boughtItem.article === item.article)
    return (
        <div className="card-wrapper" >
            <a href="#"><img src={url} alt={name} /></a>
            <div className="product-description">
                <p className="name">{name}</p>
                <div className="name-article">
                    <p className="product_price">{price}</p>
                    <p className="article">{article}</p>
                </div>
                <p className="product-color">{color}</p>
                <Heart id={article}
                    onClick={() => {
                        handleCurrentProduct(item, item.article)
                }}
                    className={`heart_icon ${isActiveHeart.includes(item.article) ? 'active-heart' : ''} `}
                />
            </div>
            <Button
                click={() => {
                    handleModal(item)
                    toBuyCurrrent(item)
                    counter(item)
            }} colored>{added===true ? "Додати ще..." : "Додати у кошик"}</Button>
        </div>
    )
}

CardItem.defaultProps = {
    isActiveHeart: false
}


CardItem.propTypes = {
    item: PropTypes.object,
    handleModal: PropTypes.func,
    handleCurrentProduct: PropTypes.func,
    toBuyCurrrent: PropTypes.func,
    isActiveHeart: PropTypes.array
}
export default CardItem