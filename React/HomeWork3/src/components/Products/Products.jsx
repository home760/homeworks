import Cards from "./Cards";
import { sendRequest } from './../../helpers/sendRequest';
import { useEffect, useState } from "react";
import './Products.scss'
import FirstModal from "../Modal/FirstModal";
import Hero from "../Hero/Hero";


const Products = ({ classNames, handleFavorite, buyClick, isActiveHeart, counter, bought}) => {

    const [products, setProducts] = useState([])
    const [currentProduct, setCurrentProduct] = useState({})
    const [isOpen, setIsOpen] = useState(false)
    const[forBuyCard, setToBuyCard] = useState({})
    const handleCurrentProduct = (cardProduct, index) => {
        setCurrentProduct(cardProduct, index)
        handleFavorite(cardProduct, index)
    }
    const toBuyCurrrent = (buyCard) => {
        setToBuyCard(buyCard)
    }
console.log(forBuyCard)
    const handleModal = () => setIsOpen(!isOpen)

    useEffect(() => {
        sendRequest('/data.json')
            .then((results ) => {
                setProducts(results)
            })
    }, [])

    return (
        <>
            <main className="main">

                <Hero/>
                <div className="conteiner products-wrapper">

                    <Cards
                        date={products}
                        classNames={classNames}
                        handleModal={handleModal}
                        toBuyCurrrent={toBuyCurrrent}
                        handleCurrentProduct={handleCurrentProduct}
                        isActiveHeart={isActiveHeart}
                        counter={() => counter(forBuyCard)}
                        bought={bought}
                    />
                    <FirstModal
                        isOpen={isOpen}
                        handleClose={handleModal}
                        clickFirst={() => {
                            handleModal()
                            buyClick(forBuyCard)
                        } }
                        title={forBuyCard.name}
                        source={forBuyCard.url}
                        article={forBuyCard.article}
                        price={forBuyCard.price}
                        color={forBuyCard.color}
                        desc={forBuyCard.desc}
                        textFirst='Додати в кошик'
                    />
                </div>
            </main>
        </>

    )
}
export default Products
