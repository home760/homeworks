import { Link } from "react-router-dom"
import CustumerButtons from "./CustumerButtons/CustumerButtons"
import Logo from "./Logo/Logo"
import Navigation from "./Navigation/Navigation"
import './Header.scss'
import  {ReactComponent as Logoicon} from "./Logo/logo.svg"

const Header = ({ favorites, toBuy }) => {
    return (
        <header className="header">
            <div className="conteiner">
                <div className="header-wrapper">
                <Logo>
                    <Link to="/" >
                        <Logoicon/>
                    </Link>
                </Logo>
                    <CustumerButtons
                        favorites={favorites}
                        toBuy={toBuy}
                    />
                        
              
                </div>
                
                <Navigation />
                
            </div>
            
            </header>
    )
}
export default Header