
const Logo = ({children}) => {
    return (
        <div className="logo-wrapper">
            {children}
        </div>
    )
}

export default Logo