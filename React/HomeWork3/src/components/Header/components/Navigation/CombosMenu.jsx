import cx from "classnames"
const CombosMenu = ({classNames, children}) => {
    return (
        <li className={cx("nav-menu-item", classNames)}>
            <a href="#">
                {children}
            </a>
        </li>
    )
}
export default CombosMenu