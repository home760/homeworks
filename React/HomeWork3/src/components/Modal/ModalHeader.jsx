import PropTypes from 'prop-types'
const ModalHeader = ({ text, children, source }) => {
    return (
        <div className="modal-header">
            <figure className="img">
                <img src={source} alt='img' />
            </figure>
            <h3>
                {text}
            </h3>
            {children}
        </div>
    )
}
ModalHeader.propTypes = {
    text: PropTypes.string,
    source: PropTypes.string,
}
export default ModalHeader