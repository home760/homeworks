import PropTypes from 'prop-types';
const ModalBody = ({ article, price, color, desc }) => {
    return (
        <div className="modal-body">
            {desc && <p>{desc}</p>}
            <div className='info'>
                <p className='price'>{price}</p>
                <div className='color-article'>
                    <p>{color}</p>
                    <p>{article }</p>
                </div>
            </div>
        </div>
    )
}
ModalBody.protoTypes = {
    text: PropTypes.string
}
export default ModalBody