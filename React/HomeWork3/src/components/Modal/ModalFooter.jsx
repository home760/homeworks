import Button from "../Button/Button"
import PropTypes from 'prop-types'
const ModalFooter = (props) => {
    const {
        clickFirst,
        textFirst,
        clickSecond,
        textSecond,
    } = props
    return (
        <div className="modal-footer">
            {textFirst && <Button colored click={clickFirst} children={textFirst}/>}
            {textSecond && <Button monochrom click={clickSecond} children={textSecond}/>}
        </div>
    )
}
export default ModalFooter