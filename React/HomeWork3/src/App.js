import { Route, Routes } from 'react-router-dom';
import './reset.scss'
import { useState, useEffect } from 'react';
import './App.css';
import Header from './components/Header/components/Header';
import Footer from './components/Footer/Footer';
import Products from './components/Products/Products';
import ErrorPage from './pages/Error/ErrorPage';
import FavoritesPage from './pages/FavoritesPage/FavoritesPage';
import LayoutPage from './pages/Layout/LayoutPage';
import OrderPage from './pages/OrderPage/OrderPage';



const App = () => {
  const data = JSON.parse(localStorage.getItem('favorites') || '[]');
  const basket = JSON.parse(localStorage.getItem('bought') || '[]');
  const count = JSON.parse(localStorage.getItem('quantity') || 0)
  
  const [favorites, setFavorite] = useState([...data])
  const [bought, setBought] = useState([...basket])
  const [quantity, setQuantity] = useState(count)

  useEffect(() => {
    const handleLocal = () => {
      localStorage.setItem('favorites', JSON.stringify(favorites))
      localStorage.setItem('bought', JSON.stringify(bought))
    }

    handleLocal()
  }, [favorites, bought])

  useEffect(() => {
    localStorage.setItem('quantity', JSON.stringify(quantity))
  }, [quantity])
  

  const hearts = JSON.parse(localStorage.getItem('isActiveHeart') || '[]');
    
  const [isActiveHeart, setActiveHeart] = useState([...hearts])

  useEffect(() => {
      localStorage.setItem('isActiveHeart', JSON.stringify(isActiveHeart))
  }, [isActiveHeart])

const counter = (item) => setQuantity(quantity+1)
  
  const handleBought = (item) => {
    const isAdded = bought.some((nexBought) => nexBought.article === item.article)
    if (!isAdded) {
      setBought([...bought, item]);
    
    }
  }
  


  const handleRemoveBought = (item) => {
    const newBought = bought.filter((product) => product.article !== item.article)
    setBought(newBought)
  }

  const handleFavorite = (test, index) => {
    const isAdded = favorites.some((favorite) => favorite.article === test.article)

    if (isAdded) {
      const existFavorites = favorites.filter((favorite) => favorite.article !== test.article);
      const existHearts = isActiveHeart.filter((added) => added !== index)
      setFavorite(existFavorites);
      setActiveHeart(existHearts)
    
    } else {
      setFavorite([...favorites, test]);
      setActiveHeart([...isActiveHeart, index])
    }
    console.log(index)
    console.log(isActiveHeart)
  }
// const handleUnFavorites = (item) => handleFavorite(item)
// (item)
  return (
    <>
      <div className="wrapper">

      <Header
        favorites={favorites.length}
        toBuy = {bought.length}
      />
        <Routes>
          <Route element={<LayoutPage/>}>
            <Route path="/" element={<Products
                  handleFavorite={handleFavorite}
                  buyClick={handleBought}
              isActiveHeart={isActiveHeart}
              counter={counter}
              bought={bought}
            />} />
            <Route path="order" element={<OrderPage
              bought={bought}
              handleRemoveBought={handleRemoveBought}
              isActiveHeart={isActiveHeart}
              handleFavorite={handleFavorite}
              quantity={quantity}
              
            />} />
            <Route path="favorites" element={<FavoritesPage
              counter={counter}
              handleUnFavorites={handleFavorite}
              favorites={favorites}
              buyClick={handleBought}
              bought={bought}
            />} />
            <Route path="*" element={<ErrorPage/>} />
          </Route>
      </Routes>
      <Footer />
      </div>
    </>
    )
 
  }

export default App
