import OrderItem from "./OrderItem"
import './Order.scss'
import FirstModal from "../../components/Modal/FirstModal"
import { useState } from "react"

const OrderPage = ({
    bought,
    handleRemoveBought,
    isActiveHeart, 
    handleFavorite,
    quantity,
    counter
}) => {
    const [removeItem, setRemoveItem] = useState({})
    const currentRemove = (removeCard) => {
        setRemoveItem(removeCard)
    }

    const [currentProduct, setCurrentProduct] = useState({})

    const [isOpen, setIsOpen] = useState(false)
    const handleModal = () => setIsOpen(!isOpen)

    const handleCurrentProduct = (cardProduct, index) => {
            setCurrentProduct(cardProduct, index)
            handleFavorite(cardProduct, index)
        }

console.log(currentProduct)
    const order = bought.map((item) => <OrderItem
        item={item}
        key={item.article}
        handleRemoveBought={()=>handleRemoveBought(removeItem)}
        currentRemove={currentRemove}
        handleModal={handleModal}
        isActiveHeart={isActiveHeart}
        handleCurrentProduct={handleCurrentProduct}
        quantity={quantity}
        counter={counter}
        // currentAddFav={currentAddFav}
    />)
    return (
        <>
            <div className="bought-wrapper">
                <h3 className="title">Кошик</h3>
           {order.length > 0 ? order : <p>Кошик порожній.</p>}
            </div>
            <FirstModal
                isOpen={isOpen}
                    handleClose={handleModal}
                    clickFirst={() => {
                        handleModal()
                        handleRemoveBought(removeItem)
                    } }
                    title={removeItem.name}
                    source={removeItem.url}
                    article={removeItem.article}
                    price={removeItem.price}
                    color={removeItem.color}
                    desc={removeItem.desc}
                    textFirst='Видалити з кошика'
            />
        </>
    )
}
export default OrderPage