import Button from '../../components/Button/Button'
import { ReactComponent as Heart } from './heart.svg'
import './Order.scss'

const OrderItem = ({ handleModal, item, currentRemove, isActiveHeart, handleCurrentProduct, quantity }) => {
    const { article, color, name, price, url } = item 
    return (
        <div className="bought-item conteiner">
            <div className="bought-item-img">
                <img src={url} alt={name} />
            </div>
            <div>
                <p>{name}</p>
                <p>{article}</p>
                <p>{color}</p>
            </div>
            <div className="buttons-wrap">
                <p>{quantity}</p>
                <Heart id={article}
                    onClick={() => {
                        handleCurrentProduct(item, item.article)
                    }}
                    className={`heart_icon_ ${isActiveHeart.includes(item.article) ? 'active-heart' : ''} `}
                />
                <div>
                    <p>{price}</p>
                    <Button colored click={() => {
                        handleModal(item)
                        currentRemove(item)

                    }
                        } >Видалити з кошика</Button>
                </div>
            </div>
        </div>
    )
}
OrderItem.defaultProps = {
    isActiveHeart: false
}
export default OrderItem