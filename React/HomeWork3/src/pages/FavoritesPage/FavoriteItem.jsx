import Button from '../../components/Button/Button'
import './Favorite.scss'

const FavoriteItem = ({ handleUnFavorites, favorite, toBuyCurrrent, bought, counter}) => {
    const { article, color, name, price, url } = favorite 
    const added = bought.some((boughtItem) => boughtItem.article === favorite.article)
    return (
        <div className="favorite-item conteiner">
            <div className="favorite-item-img">
                <img src={url} alt={name} />
            </div>
            <div className="info">
                <p>{name}</p>
                <p>{article}</p>
                <p>{color}</p>
            </div>
            <div>

                <Button colored click={() => {
                    toBuyCurrrent(favorite)
                    counter(favorite)
                }} >{added === true ? 'Додати ще...' : 'Додати в кошик'}</Button>
            </div>
            <div>
                <Button colored click={() => handleUnFavorites(favorite, article)} >Видалити з улюбленого</Button>
            </div>
                <p>{price}</p>
        </div>
    )
}
export default FavoriteItem