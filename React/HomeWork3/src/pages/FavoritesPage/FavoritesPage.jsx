
import FavoriteItem from "./FavoriteItem";
import { useState } from "react";

const FavoritePage = ({handleUnFavorites, favorites, buyClick, bought, counter}) => {


    const[forBuyCard, setToBuyCard] = useState({})    
    const toBuyCurrrent = (forBuyCard) => {
        setToBuyCard(forBuyCard)
        buyClick(forBuyCard)
        }

    const favCard = favorites.map((favorite) => (

        <FavoriteItem
            favorite={favorite}
            key={favorite.article}
            handleUnFavorites={handleUnFavorites}
            toBuyCurrrent={toBuyCurrrent}
            bought={bought}
            counter={counter}
    
        /> )
)
    return (
        <div className="favorites-wrapper">
            <h3 className="title">Улюблены товари</h3>
            {favCard.length > 0 ? favCard : <p>Список улюбленого порожній.</p>}
        </div>

    )
}

export default FavoritePage