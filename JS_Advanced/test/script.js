mport {sendRequest} from '../functions/sendRequest.js';
import {delToDoSuccess, delToDoError, addedToDoSuccess} from "./alert.js";
import {API} from "../configs/API.js";

const MAX_TODOS = 184;

export default class TodoForm {
	constructor(userId) {
		this.userId = userId;
	}

	sendTodoData(form) {
		form.addEventListener('submit', (event) => {
			event.preventDefault();
			let inputTask = event.target.querySelector('[name="taskName"]').value;

			let background = document.querySelector('.modal-backdrop') // Находим элементы по модалкам что бы его убрать после отправки данных
			let modal = document.querySelector('.modal') // Находим элементы по модалкам что бы его убрать после отправки данных

			if (document.body.classList.contains('modal-open') || background || modal) { // проверям и удаляем все что связано по модалке
				document.body.classList.remove('modal-open')
				background.remove();
				modal.remove();
			}

			if (inputTask !== '') {
				sendRequest(`${API}todos`, 'POST', { // Добаивть новый todos
					body: JSON.stringify({
						userId: this.userId,
						title: inputTask,
					})
				})
					.then(data => {
						// console.log('addTodo', data);

						if (data) {
							const card = document.querySelector(`[data-user-id="${this.userId}"]`);
							const cardBody = card.querySelector('.card-body');
							const taskList = cardBody.querySelector('.task-list');

							const tasksList = document.createElement('ul');
							tasksList.classList.add('list-group', 'task-list');

							if (taskList) { // нам нужна проверка на наличиие списка, если у пользователя уже есть тудушки то нужно добавить в существующий список, если нет то создать
								taskList.insertAdjacentHTML('afterbegin', `
                                    <li class="list-group-item task-list--item list-group-item-action pr-5" data-id="${data.id}">
                                        <span class="icon-del">
                                            <svg fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24px" height="24px">
                                            <path	d="M 10.806641 2 C 10.289641 2 9.7956875 2.2043125 9.4296875 2.5703125 L 9 3 L 4 3 A 1.0001 1.0001 0 1 0 4 5 L 20 5 A 1.0001 1.0001 0 1 0 20 3 L 15 3 L 14.570312 2.5703125 C 14.205312 2.2043125 13.710359 2 13.193359 2 L 10.806641 2 z M 4.3652344 7 L 5.8925781 20.263672 C 6.0245781 21.253672 6.877 22 7.875 22 L 16.123047 22 C 17.121047 22 17.974422 21.254859 18.107422 20.255859 L 19.634766 7 L 4.3652344 7 z"></path>
                                            </svg>
                                        </span>
                                        <span class="">${inputTask}</span>
                                    </li>
                                `)
								addedToDoSuccess()
							} else {
								tasksList.insertAdjacentHTML('afterbegin', `
                                    <li class="list-group-item task-list--item list-group-item-action pr-5" data-id="${data.id}">
                                        <span class="icon-del">
                                            <svg fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24px" height="24px">
                                                <path	d="M 10.806641 2 C 10.289641 2 9.7956875 2.2043125 9.4296875 2.5703125 L 9 3 L 4 3 A 1.0001 1.0001 0 1 0 4 5 L 20 5 A 1.0001 1.0001 0 1 0 20 3 L 15 3 L 14.570312 2.5703125 C 14.205312 2.2043125 13.710359 2 13.193359 2 L 10.806641 2 z M 4.3652344 7 L 5.8925781 20.263672 C 6.0245781 21.253672 6.877 22 7.875 22 L 16.123047 22 C 17.121047 22 17.974422 21.254859 18.107422 20.255859 L 19.634766 7 L 4.3652344 7 z"></path>
                                            </svg>
                                        </span>
                                        <span class="text">${inputTask}</span>
                                    </li>
                                `)
								addedToDoSuccess()
								cardBody.append(tasksList)

								tasksList.addEventListener('click', (event) => {
									const listTarget = event.target.closest("li");
									const deleteIcon = event.target.closest(".icon-del");

									if (deleteIcon) {
										const toDelete = confirm('Remove task?');
										const id = deleteIcon.closest("li").getAttribute("data-id")

										if (toDelete) {
											if (id <= MAX_TODOS) { // делаем такую проверку потому что в баззу у нас добавлется 185 туду и запрос на него нельзя сделать
												sendRequest(`${API}/todos/${id}`, "DELETE")
													.then(data => {
														if (data) {
															deleteIcon.closest('.task-list--item').remove();
															delToDoSuccess()
														}
													})
											} else {
												deleteIcon.closest('.task-list--item').remove();
												delToDoSuccess()
											}
										}
									}

									if (listTarget && !deleteIcon) {
										listTarget.classList.toggle("task-list--item-completed");
									}
								})
							}
						}
					})
			}
		})
	}

	render() {
		this.formElem = document.createElement('form')
		this.formElem.innerHTML = `
            <div class="form-group">
                <label class="form-label" style="width: 100%;">
                    <p>Add new task</p>
                    <input id="taskName" class="form-control" name="taskName" placeholder="Task Name">
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Assign</button>
        `
		this.sendTodoData(this.formElem);
		return this.formElem;
	}
}
