// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// доречно використовувати коли розробник не знає які дані будуть надходити. за допомогою методу можна передбачити помилки, або порожні строки і не виводити на сторінку дані, або виводити частково. При цьому сайт буде працювати коректно.




const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

const div = document.getElementById('root');
const ul = document.createElement('ul');
div.append(ul);   

function root(item) {
    const li = document.createElement('li');
    ul.append(li);
    li.innerText = `Назва: ${item.name} Автор: ${item.author} Ціна: ${item.price} грн;`
}
for (let item of books) {
    const { name, author, price } = item;
    const test = ['name', 'author', 'price']
  let itemKeys = Object.keys(item)
    try {
        if (name && author && price) {
            root(item)
        } else {
          let epsent = test.filter(elem => !itemKeys.includes(elem));
          throw new Error(`В пункті номер ${books.indexOf(item)+1} відсутня ${epsent}`)
        }
    }
    catch (error) {
        console.error(error);
    }
}