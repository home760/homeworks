    
const API_users = 'https://ajax.test-danit.com/api/json/users';
const API_posts = 'https://ajax.test-danit.com/api/json/posts'
const main = document.querySelector('.main')
async function sendRequest(url, method, options) {
    const response = await fetch(url, { method: method, ...options })
    const result =  response.json() // data 
    return result
}
class Card {
    constructor() {

    }
    async render() {
        let users = await this.renderPageUser()
        let posts = await this.renderPagePost()
        users.map(user => {
            const { name, email, id } = user
            posts.map(post => {
                const { title, body, userId, id:postId } = post
                if (id === userId) {
                    const wrapper = document.createElement('div')
                    wrapper.classList.add('wrapper')
                    const titleCard = document.createElement('h3')
                    const author = document.createElement('p')
                    const emailCard = document.createElement('p')
                    const bodyCard = document.createElement('p')
                    const button = document.createElement('button')
                    main.append(wrapper)
                    wrapper.append(titleCard)
                    wrapper.append(author)
                    wrapper.append(emailCard)
                    wrapper.append(bodyCard)
                    wrapper.append(button)
                    button.innerText = 'DELETE'
                    button.setAttribute('id', postId)
                    titleCard.innerText = title.toUpperCase()
                    author.innerText = name
                    emailCard.innerText = email
                    bodyCard.innerText = body
                }МС 
            })
        })

    }
    async renderPageUser() {
        const users = await sendRequest(API_users)
        return users
    }
    async renderPagePost() {
        const posts = await sendRequest(API_posts)
        return posts
    }

    delete() {
        main.addEventListener('click', (event) => {
            console.log(event.target.type)
            if (event.target.type == 'submit') {
                event.target.closest('.wrapper').remove()
                fetch(`https://ajax.test-danit.com/api/json/posts/${event.target.id}`, {
                    method: 'DELETE',
                })
                    .then(response => console.log(response))
            
            
            }     
        })
    }
}

let card = new Card()


card.renderPageUser()
card.renderPagePost()
card.render() 
card.delete()