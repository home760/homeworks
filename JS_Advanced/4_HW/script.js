const AJAX = 'https://ajax.test-danit.com/api/swapi/films'
const list = document.getElementById('list')
const loading = document.querySelector('.loading')
loading.style.display = "block" 
document.addEventListener("DOMContentLoaded", (event) => {
   
    
    




    function sendRequest(url) {
        return fetch(url)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Error')
                } {
                
                    return response.json()
                }
            })
    }

    const allData = sendRequest(AJAX)


    allData
        .then(data => {
            data.forEach((films) => {
                
                const { id, name, openingCrawl, characters } = films
               const h = document.createElement('h3')
            const actorsP = document.createElement('p')
            const description = document.createElement('p')
            actorsP.style.fontStyle = 'Italic'
            actorsP.style.fontSize = '12px'
            list.append(h)
            list.append(actorsP)
            list.append(description)
            h.innerText = `${id} ${name}`
            description.innerText = openingCrawl
                const actorsPlace = document.querySelector('.actors')
                characters.forEach((actorsUrl) => {
                    loading.style.display = "none" 
                    sendRequest(actorsUrl)
                        .then((actors) => {
                            
                            const{name:actorsName} = actors
                            console.log(actorsName)
                            const span = document.createElement('span')
                            span.textContent = `${actorsName}, `
                            actorsP.append(span)
                            
                            return actorsName
                            
                        })

                })
            })
        })    

    .catch((error) => console.log(error))
 
})