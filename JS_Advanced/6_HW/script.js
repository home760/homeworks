
const wrapper = document.querySelector('.wrapper')
const button = document.querySelector('.button');
const adressWrapper = document.querySelector('.adress_wrapper');
const API = 'https://api.ipify.org/?format=json'
const IP = 'http://ip-api.com/json/'

async function sendRequest(url, method, options) {
    const response = await fetch(url, { method: method, ...options })
    const result =  response.json() // data 
    return result
}
class Adress{
    constructor() {
    }
    getId() {
        button.addEventListener('click', async () => {
            const data = await sendRequest(API)
            console.log(data.ip)
            let ad = await this.getAdress(data.ip)
            const {timezone, country, regionName, city } = ad
            console.log(timezone, country, regionName, city)
            this.render(timezone, country, regionName, city)
        })
    }
    async getAdress(ip) {
        const adress = await sendRequest(`${IP}${ip}`)
        return adress
    }
    render(timezone, country, regionName, city) {
        const p = document.createElement('p');
        p.classList.add('adress')
        adressWrapper.append(p)
        p.innerText = `Континент: ${timezone}, країна: ${country}, регіон: ${regionName}, місто: ${city}`

    }
}
    
let adress = new Adress
adress.getId() 
