// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//створення обєкта на основі вже існуючого і доповнення необхідними методами і даними
// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// щоб наслідувати змінні у батьківського класу




class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return `name: ${this._name}`;
    }
    set name(value) {
        this._name = value;
    }
    get age() {
        return `age: ${this._age}`;
    }
    set age(value) {
        this._age = value;
    }
    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value
    }
}


class Programmer extends Employee {
    constructor(name, age, salary, lang = 'ukr') {
        super(name, age, salary);
        this._lang = lang;
    }
    
    get salary() {
       return  `salary: ${this._salary * 3}`;
    }
    set salary(value) {
        this._salary;
    }   
}

const user = new Programmer('Ivan', 37, 20000);
console.log(user.name);
console.log(user.age)
console.log(user.salary);

const user1 = new Programmer('Daria', 36, 60000);
console.log(user1.name);
console.log(user1.age)
console.log(user1.salary);
